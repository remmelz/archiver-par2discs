#!/bin/bash

###############################
# Default parameters
###############################

_target=$1
_medium="dvd"
_redundancy=10

###############################
# Checking parameters
###############################
if [[ -z $1 ]]
then
  _script=$(basename $0)
  echo "Usage: ${_script} <file|folder> <medium [dvd|bluray]> <redundancy>"
  exit 1
fi

[[ -n $2 ]] && _medium=$2
[[ -n $3 ]] && _redundancy=$3

[[ ${_medium} == 'dvd'    ]] && _size=4100
[[ ${_medium} == 'bluray' ]] && _size=22000
[[ ${_medium} == 'test'   ]] && _size=50

###############################
# No splitting
###############################
if [[ -d ${_target} ]]
then
  cd ${_target} || exit 1
  for f in $(ls); do
    echo "par2create -r${_redundancy} $f"
    par2create -r${_redundancy} $f
    [[ $? -gt 0 ]] && exit
  done
  mkdir parfiles
  mv *.par2 ./parfiles
  sha256sum * */* > sha256sum.txt 2> /dev/null
  _foldersize=$(du -BM -s . | awk -F' ' '{print $1}' | sed 's/M//g')
  if [[ ${_foldersize} -gt ${_size} ]]
  then
    echo "Warning: exceeding disc size."
  fi
  exit 0
fi

###############################
# Checking target
###############################
if [[ ! -f ${_target} ]]
then
  echo "Error: could not find target file."
  exit 1
fi

cd $(dirname ${_target}) || exit 1

###############################
# Split file
###############################
_filesize=$(du -s ${_target} | awk -F' ' '{print $1}')

if [[ -z ${_size} ]]
then
  echo "Error: unkown medium."
  exit 1
fi


###############################
# Creating disc folders
###############################
if [[ $(echo "${_filesize}/1024" | bc) -gt ${_size} ]]
then
  echo 'Splitting file...'
  split --bytes=${_size}MB ${_target}
  n=0
  for f in $(ls -1 x??)
  do
    let n=$n+1
    mkdir -v ${_medium}$n
    mv -v ${f} ./${_medium}${n}/${_target}.part${n}
  done
else
  n=1
  mkdir -v ${_medium}1
  cp -v ${_target} ${_medium}${n}
fi


###############################
# Creating par2 files
###############################
c=1
while [[ $c -le $n ]]
do
  cd ./${_medium}${c}
  par2create -r${_redundancy} ${_target} *
  mkdir -v ./par2${_medium}${c}
  mv -v *.par2 ./par2${_medium}${c}
  let c=$c+1
  cd ..
done


###############################
# Move par2 files to next disc
###############################
c=0
m=1
echo
while [[ $c -lt $n ]]
do
  let c=$c+1
  let m=$m+1
  [[ $c -eq $n ]] && m=1
  mv -v ./${_medium}${c}/par2${_medium}${c} ./${_medium}${m}
done


###############################
# Create sha256sum files
###############################
echo
c=1
while [[ $c -le $n ]]
do
  cd ./${_medium}${c}
  echo "Creating sha256sum for ${_medium}${c}..."
  sha256sum * */* > sha256sum.txt 2> /dev/null
  cd ..
  let c=$c+1
done


###############################
# Fill up empty space
###############################

echo
echo "Checking empty space on disc${n}."
_discsize=$(du -s ${_medium}1 | awk -F' ' '{print $1}')
_spacereq=$(du -s ${_medium}1/par2test${n} | awk -F' ' '{print $1}')

c=1
d=2
p=1
while [[ $c -le $n ]]
do
  _spaceuse=$(du -s ${_medium}${n} | awk -F' ' '{print $1}')
  _spaceava=$(echo ${_discsize}-${_spaceuse} | bc)

  [[ ${_spacereq} -gt ${_spaceava} ]] && break

  if [[ ! -d ./${_medium}${n}/par2${_medium}${p} ]]
  then
    echo "-> Copying extra par2 files for disc${d} to disc${n}..."
    cp -R ./${_medium}${d}/par2${_medium}${p} ./${_medium}${n}/
  fi

  [[ $p -eq $n ]] && p=0
  [[ $d -eq $n ]] && d=0

  let c=$c+1
  let d=$d+1
  let p=$p+1
done

###############################
# Show results
###############################

echo
echo "Discs ready."
c=1
while [[ $c -le $n ]]
do
  du -hs ${_medium}${c}
  let c=$c+1
done
echo

exit 0
